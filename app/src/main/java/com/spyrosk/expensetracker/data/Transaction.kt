package com.spyrosk.expensetracker.data

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.spyrosk.expensetracker.utils.CurrencyFormatter
import com.spyrosk.expensetracker.utils.currentDateInMillis
import com.spyrosk.expensetracker.utils.formatMedium
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal

@Entity(tableName = "transactions",
		foreignKeys = [ForeignKey(
			entity = Category::class,
			parentColumns = arrayOf("id"),
			childColumns = arrayOf("category_id"),
			onDelete = ForeignKey.CASCADE)]
)
@Parcelize
data class Transaction(
	val title: String? = null,
	val transactionType: TransactionType,
	@ColumnInfo(name = "amount_in_cents") val amount: BigDecimal,
	val dateTimestamp: Long = currentDateInMillis(),
	@ColumnInfo(index = true) val category_id: Int,
	@PrimaryKey(autoGenerate = true) val id: Int = 0
) : Parcelable {
	val dateTimestampFormatted: String
		get() = formatMedium(dateTimestamp)
	
	val amountFormatted: String
		get() = CurrencyFormatter.format(amount, isNegative = (transactionType == TransactionType.EXPENSE))
}
