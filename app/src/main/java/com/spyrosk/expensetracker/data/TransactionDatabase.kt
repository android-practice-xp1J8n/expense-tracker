package com.spyrosk.expensetracker.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.spyrosk.expensetracker.di.ApplicationScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Provider

@Database(entities = [Transaction::class, Category::class], version = 1)
@TypeConverters(Converters::class)
abstract class TransactionDatabase : RoomDatabase() {
	
	abstract fun transactionDao(): TransactionDao
	
	abstract fun categoryDao(): CategoryDao
	
	abstract fun transactionWithCategoryDao(): TransactionWithCategoryDao
	
	// Inner classes
	
	class Callback @Inject constructor(
		private val transactionDatabase: Provider<TransactionDatabase>,
		@ApplicationScope private val applicationScope: CoroutineScope,
		private val categorySeeder: CategorySeeder
	) : RoomDatabase.Callback() {
		
		override fun onCreate(db: SupportSQLiteDatabase) {
			super.onCreate(db)
			
			val categoryDao = transactionDatabase.get().categoryDao()
			
			applicationScope.launch {
				for(category in categorySeeder.getDefaultCategories())
					categoryDao.insert(category)
			}
			
		}
		
	}
	
}
