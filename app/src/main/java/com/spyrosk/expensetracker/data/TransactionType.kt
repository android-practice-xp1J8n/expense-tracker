package com.spyrosk.expensetracker.data

enum class TransactionType {
	EXPENSE,
	INCOME
}