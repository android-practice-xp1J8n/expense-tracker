package com.spyrosk.expensetracker.data

import androidx.room.Embedded
import androidx.room.Relation
import androidx.room.TypeConverters

@TypeConverters
data class TransactionWithCategory (
	@Embedded val transaction: Transaction,
	@Relation(parentColumn = "category_id", entityColumn = "id") val category: Category
)
