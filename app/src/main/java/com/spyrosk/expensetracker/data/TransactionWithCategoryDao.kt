package com.spyrosk.expensetracker.data

import androidx.room.Dao
import androidx.room.RawQuery
import androidx.room.Transaction
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal

@Dao
interface TransactionWithCategoryDao {
	
	@Transaction
	@RawQuery(observedEntities = [TransactionWithCategory::class])
	fun get(query: SupportSQLiteQuery): Flow<List<TransactionWithCategory>>
	
	fun get(
		categoryIds: Collection<Int>? = null,
		amountMin: BigDecimal? = null,
		amountMax: BigDecimal? = null,
		dateStart: Long? = null,
		dateEnd: Long? = null,
		orderDescending: Boolean = true,
		limit: Int? = null
	): Flow<List<TransactionWithCategory>> {
		val builder = StringBuilder()
		
		builder.appendWithNewLine("select transactions.* from transactions")
			.appendWithNewLine("left join categories on transactions.category_id = categories.id")
		
		// This is just so that the following clauses can simply use "and"
		builder.appendWithNewLine("where 1=1")
		
		if(!categoryIds.isNullOrEmpty())
			builder.appendWithNewLine("and categories.id in ${categoryIds.toQueryCompatibleString()}")
		
		val convertersInstance = Converters()
		if(amountMin != null)
			builder.appendWithNewLine("and amount_in_cents >= ${convertersInstance.decimalCurrencyToLongCents(amountMin)}")
		if(amountMax != null)
			builder.appendWithNewLine("and amount_in_cents <= ${convertersInstance.decimalCurrencyToLongCents(amountMax)}")
		
		if(dateStart != null)
			builder.appendWithNewLine("and dateTimestamp >= $dateStart")
		if(dateEnd != null)
			builder.appendWithNewLine("and dateTimestamp <= $dateEnd")
		
		val order = if(orderDescending) "desc" else "asc"
		builder.appendWithNewLine("order by dateTimestamp $order, id $order")
		
		if(limit != null)
			builder.appendWithNewLine("limit $limit")
		
		val query = SimpleSQLiteQuery(builder.toString())
		return get(query)
	}
	
}

private fun StringBuilder.appendWithNewLine(s: String): StringBuilder =
	this.append(s).append("\n")

private fun <T> Collection<T>.toQueryCompatibleString(): String =
	this.toString().replace("[", "(").replace("]", ")")
