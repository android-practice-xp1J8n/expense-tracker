package com.spyrosk.expensetracker.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.spyrosk.expensetracker.data.CategorySeeder
import com.spyrosk.expensetracker.data.TransactionDatabase
import com.spyrosk.expensetracker.utils.ResourceProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
	
	@Provides
	@Singleton
	fun provideTransactionDatabase(app: Application, callback: TransactionDatabase.Callback) =
		Room.databaseBuilder(app, TransactionDatabase::class.java, "transaction_database")
			.fallbackToDestructiveMigration()	// drops and re-creates the table if no strategy has been declared
			.addCallback(callback)
			.build()
	
	@Provides
	fun provideTransactionDao(database: TransactionDatabase) = database.transactionDao()
	
	@Provides
	fun provideCategoryDao(database: TransactionDatabase) = database.categoryDao()
	
	@Provides
	fun provideTransactionWithCategoryDao(database: TransactionDatabase) = database.transactionWithCategoryDao()
	
	@Provides
	@Singleton
	@ApplicationScope
	fun provideApplicationScope() = CoroutineScope(SupervisorJob())
	
	@Provides
	@Singleton
	fun provideResourceProvider(@ApplicationContext context: Context) = ResourceProvider(context)
	
	@Provides
	@Singleton
	fun provideCategorySeeder(resourceProvider: ResourceProvider) = CategorySeeder(resourceProvider)
	
}
