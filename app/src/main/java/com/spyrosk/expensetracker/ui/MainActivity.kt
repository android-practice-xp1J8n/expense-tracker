package com.spyrosk.expensetracker.ui

import android.os.Bundle
import android.view.KeyCharacterMap
import android.view.KeyEvent
import android.view.ViewConfiguration
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.spyrosk.expensetracker.R
import com.spyrosk.expensetracker.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
	
	private lateinit var navController: NavController
	private lateinit var binding: ActivityMainBinding
	
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		
		binding = ActivityMainBinding.inflate(layoutInflater)
		setContentView(binding.root)
		
		adjustBottomPadding()
		setupActionBar()
	}
	
	private fun adjustBottomPadding() {
		val navigationBarExists =
			!ViewConfiguration.get(this).hasPermanentMenuKey() &&
			!KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK)
		
		if(navigationBarExists) {
			with (binding.root) {
				setPadding(paddingLeft, paddingTop, paddingRight, 0)
			}
		}
	}
	
	private fun setupActionBar() {
		val navHostFragment =
			supportFragmentManager.findFragmentById(R.id.nav_host_fragment_container) as NavHostFragment
		navController = navHostFragment.findNavController()
		
		setupActionBarWithNavController(navController)
	}
	
	override fun onSupportNavigateUp(): Boolean {
		// This takes care of back - if it returns false, super is called
		return navController.navigateUp() || super.onSupportNavigateUp()
	}
	
}
