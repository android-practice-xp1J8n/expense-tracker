package com.spyrosk.expensetracker.ui.filters

import android.app.Dialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import com.spyrosk.expensetracker.R
import com.spyrosk.expensetracker.data.Category
import com.spyrosk.expensetracker.databinding.FragmentFiltersBinding
import com.spyrosk.expensetracker.ui.filter_date.*
import com.spyrosk.expensetracker.utils.CurrencyInputFilter
import com.spyrosk.expensetracker.utils.formatMedium
import com.spyrosk.expensetracker.utils.getColorOpt
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.math.BigDecimal
import com.spyrosk.expensetracker.ui.filters.FiltersDialogViewModel.FiltersEvent as FiltersEvent
import com.spyrosk.expensetracker.utils.CurrencyFormatter.Companion.formatPlain as formatCurrencyPlain

@AndroidEntryPoint
class FiltersDialogFragment : DialogFragment() {
	
	private val viewModel: FiltersDialogViewModel by viewModels()
	private lateinit var binding: FragmentFiltersBinding
	
	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		binding = FragmentFiltersBinding.inflate(layoutInflater)
		
		return AlertDialog.Builder(ContextThemeWrapper(requireContext(), R.style.CustomDialogStyle))
			.setTitle(getString(R.string.filters_dialog_title))
			.setView(binding.root)
			.setNegativeButton(getString(R.string.cancel_text), null)
			.setPositiveButton(getString(R.string.apply_text)) { _, _ -> viewModel.applyClicked() }
			.setNeutralButton(getString(R.string.clear_all_text)) { _, _ -> viewModel.clearClicked() }
			.create()
	}
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		viewModel.availableCategoriesLiveData.observe(viewLifecycleOwner) { newCategories -> populateChips(newCategories) }
		
		binding.etAmountMax.filters = arrayOf(CurrencyInputFilter())
		viewModel.amountMax?.let { binding.etAmountMax.setText(formatCurrencyPlain(it)) }
		binding.etAmountMax.addTextChangedListener { t ->
			viewModel.amountMax = if(t.toString().isNotBlank()) BigDecimal(t.toString()) else null
		}
		
		binding.etAmountMin.filters = arrayOf(CurrencyInputFilter())
		viewModel.amountMin?.let { binding.etAmountMin.setText(formatCurrencyPlain(it)) }
		binding.etAmountMin.addTextChangedListener { t ->
			viewModel.amountMin = if(t.toString().isNotBlank()) BigDecimal(t.toString()) else null
		}
		
		updateDateText(DateFilterType.START_DATE)
		binding.tvDateStart.setOnClickListener { viewModel.dateFromClicked() }
		
		updateDateText(DateFilterType.END_DATE)
		binding.tvDateEnd.setOnClickListener { viewModel.dateToClicked() }
		
		listenToViewModelEvents()
		
		return binding.root
	}
	
	private fun updateDateText(dateFilterType: DateFilterType) {
		val date = if(dateFilterType == DateFilterType.START_DATE) viewModel.dateStart else viewModel.dateEnd
		val dateTextView = if(dateFilterType == DateFilterType.START_DATE) binding.tvDateStart else binding.tvDateEnd
		
		dateTextView.text = when(date) {
			null -> requireContext().getString(R.string.none_text)
			else -> formatMedium(date)
		}
	}
	
	private fun onDateResultReceived(dateFilterType: DateFilterType) = { _: String, bundle: Bundle ->
		val resultDate = bundle.getLong(DATE_DIALOG_FRAGMENT_RESULT_DATE_KEY)
		val dateCleared = bundle.getBoolean(DATE_DIALOG_FRAGMENT_RESULT_DATE_CLEARED_KEY)
		viewModel.dateResultReceived(dateFilterType, resultDate, dateCleared)
	}
	
	private fun listenToViewModelEvents() {
		viewLifecycleOwner.lifecycleScope.launchWhenStarted {
			viewModel.filtersEventFlow.collect { event -> handleEvent(event) }
		}
	}
	
	private fun handleEvent(event: FiltersEvent) {
		when(event) {
			is FiltersEvent.NavigateToStartDateSelection -> {
				setFragmentResultListener(DATE_DIALOG_FRAGMENT_RESULT_REQUEST_KEY, onDateResultReceived(DateFilterType.START_DATE))
				val action = FiltersDialogFragmentDirections.actionFiltersDialogFragmentToDateDialogFragment(
					dateAsLongArg = event.date,
					titleArg = requireContext().getString(R.string.filter_start_date_dialog_title))
				findNavController().navigate(action)
			}
			is FiltersEvent.NavigateToEndDateSelection -> {
				setFragmentResultListener(DATE_DIALOG_FRAGMENT_RESULT_REQUEST_KEY, onDateResultReceived(DateFilterType.END_DATE))
				val action = FiltersDialogFragmentDirections.actionFiltersDialogFragmentToDateDialogFragment(
					dateAsLongArg = event.date,
					titleArg = requireContext().getString(R.string.filter_end_date_dialog_title))
				findNavController().navigate(action)
			}
			is FiltersEvent.UpdateDateTextView -> updateDateText(event.dateFilterType)
			is FiltersEvent.NavigateBackWithResult -> {
				setResult(event.filtersCleared)
				findNavController().popBackStack()
			}
		}
	}
	
	private fun setResult(filtersCleared: Boolean) {
		val filtersResult = if(filtersCleared) {
			FiltersParcelable(filtersCleared = true)
		}
		else {
			FiltersParcelable(
				selectedCategoryIDs = viewModel.selectedCategoryIDs,
				amountMin = viewModel.amountMin,
				amountMax = viewModel.amountMax,
				dateStart = viewModel.dateStart,
				dateEnd = viewModel.dateEnd)
		}
		
		val bundle = bundleOf(FILTERS_DIALOG_FRAGMENT_RESULT_KEY_FILTERS to filtersResult)
		
		setFragmentResult(FILTERS_DIALOG_FRAGMENT_RESULT_REQUEST_KEY, bundle)
	}
	
	private fun populateChips(categories: List<Category>?) {
		val chipGroup = binding.chgCategories
		
		chipGroup.removeAllViews()	// Alternatively, check whether chips already exist
		
		if(categories.isNullOrEmpty())
			return
		
		for(category in categories) {
			val chip = Chip(chipGroup.context)
			chip.text = category.name
			
			val states = arrayOf(intArrayOf(android.R.attr.state_checked), intArrayOf(-android.R.attr.state_checked))
			val colors = intArrayOf(category.color, getColorOpt(requireContext(),R.color.light_grey))
			chip.chipBackgroundColor = ColorStateList(states, colors)
			
			chip.setTextColor(getColorOpt(requireContext(), R.color.background_inner))
			
			chip.isClickable = true
			chip.isCheckable = true
			chip.isFocusable = true
			chip.isChecked = viewModel.selectedCategoryIDs?.contains(category.id) ?: false
			
			chip.setOnCheckedChangeListener { _, isChecked ->
				if(isChecked)
					viewModel.categorySelected(category)
				else
					viewModel.categoryUnselected(category)
			}
			
			chipGroup.addView(chip)
		}
		
	}
	
}

const val FILTERS_DIALOG_FRAGMENT_RESULT_REQUEST_KEY = "FILTERS_DIALOG_FRAGMENT_RESULT_REQUEST_KEY"
const val FILTERS_DIALOG_FRAGMENT_RESULT_KEY_FILTERS = "FILTERS_DIALOG_FRAGMENT_RESULT_KEY_FILTERS"
