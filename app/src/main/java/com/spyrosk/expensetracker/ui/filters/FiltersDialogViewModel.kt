package com.spyrosk.expensetracker.ui.filters

import androidx.lifecycle.*
import com.spyrosk.expensetracker.data.Category
import com.spyrosk.expensetracker.data.CategoryDao
import com.spyrosk.expensetracker.utils.currentDateInMillis
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.math.BigDecimal
import javax.inject.Inject

@HiltViewModel
class FiltersDialogViewModel @Inject constructor(
	categoryDao: CategoryDao,
	private val savedState: SavedStateHandle
) : ViewModel() {
	
	private val amountMaxKey = "amount_max_key"
	private val amountMinKey = "amount_min_key"
	private val dateFromKey = "date_from_key"
	private val dateToKey = "date_to_key"
	
	private val filtersEventChannel = Channel<FiltersEvent>()
	val filtersEventFlow = filtersEventChannel.receiveAsFlow()
	
	val availableCategoriesLiveData = categoryDao.getAllCategories().asLiveData()
	
	private val filtersFromArgs = savedState.get<FiltersParcelable>("filters_arg")
	
	var selectedCategoryIDs: MutableSet<Int>? = filtersFromArgs?.selectedCategoryIDs	// get from args or set null
	private val categoriesObserver = Observer<List<Category>> { categories ->
			if(selectedCategoryIDs == null)
				selectedCategoryIDs = categories.map { c -> c.id }.toMutableSet()
		}
	
	var amountMax = savedState.get<BigDecimal>(amountMaxKey) ?: filtersFromArgs?.amountMax
		set(value) {
			field = value
			savedState.set(amountMaxKey, value)
		}
	
	var amountMin = savedState.get<BigDecimal>(amountMinKey) ?: filtersFromArgs?.amountMin
		set(value) {
			field = value
			savedState.set(amountMinKey, value)
		}
	
	var dateStart = savedState.get<Long>(dateFromKey) ?: filtersFromArgs?.dateStart
		set(value) {
			field = value
			savedState.set(dateFromKey, value)
		}
	
	var dateEnd = savedState.get<Long>(dateToKey) ?: filtersFromArgs?.dateEnd
		set(value) {
			field = value
			savedState.set(dateToKey, value)
		}
	
	
	init {
		// The observer has to be removed later
		availableCategoriesLiveData.observeForever(categoriesObserver)
	}
	
	override fun onCleared() {
		super.onCleared()
		availableCategoriesLiveData.removeObserver(categoriesObserver)
	}
	
	fun dateFromClicked() {
		viewModelScope.launch {
			filtersEventChannel.send(FiltersEvent.NavigateToStartDateSelection(dateStart ?: currentDateInMillis()))
		}
	}
	
	fun dateToClicked() {
		viewModelScope.launch {
			filtersEventChannel.send(FiltersEvent.NavigateToEndDateSelection(dateEnd ?: currentDateInMillis()))
		}
	}
	
	fun clearClicked() {
		viewModelScope.launch {
			filtersEventChannel.send(FiltersEvent.NavigateBackWithResult(filtersCleared = true))
		}
	}
	
	fun applyClicked() {
		viewModelScope.launch {
			filtersEventChannel.send(FiltersEvent.NavigateBackWithResult(filtersCleared = false))
		}
	}
	
	fun categorySelected(category: Category) {
		selectedCategoryIDs!!.add(category.id)
	}
	
	fun categoryUnselected(category: Category) {
		selectedCategoryIDs!!.remove(category.id)
	}
	
	fun dateResultReceived(dateFilterType: DateFilterType, resultDate: Long, dateCleared: Boolean) {
		if(dateFilterType == DateFilterType.START_DATE)
			dateStart = if(dateCleared) null else resultDate
		else
			dateEnd = if(dateCleared) null else resultDate
		
		viewModelScope.launch {
			filtersEventChannel.send(FiltersEvent.UpdateDateTextView(dateFilterType))
		}
	}
	
	// Inner classes
	
	sealed class FiltersEvent {
		data class NavigateToStartDateSelection(val date: Long) : FiltersEvent()
		data class NavigateToEndDateSelection(val date: Long) : FiltersEvent()
		data class NavigateBackWithResult(val filtersCleared: Boolean = false) : FiltersEvent()
		data class UpdateDateTextView(val dateFilterType: DateFilterType) : FiltersEvent()
		object ShowCategoriesWarning : FiltersEvent()	// TODO
		object ShowAmountWarning : FiltersEvent()		// TODO
		object ShowDateWarning : FiltersEvent()			// TODO
	}
	
}
