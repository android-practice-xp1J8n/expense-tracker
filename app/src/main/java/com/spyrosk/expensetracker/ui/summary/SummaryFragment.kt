package com.spyrosk.expensetracker.ui.summary

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.leinardi.android.speeddial.SpeedDialActionItem
import com.spyrosk.expensetracker.R
import com.spyrosk.expensetracker.data.Transaction
import com.spyrosk.expensetracker.data.TransactionWithCategory
import com.spyrosk.expensetracker.databinding.FragmentSummaryBinding
import com.spyrosk.expensetracker.ui.transactions.TransactionsAdapter
import com.spyrosk.expensetracker.utils.CurrencyFormatter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import com.spyrosk.expensetracker.ui.summary.SummaryViewModel.SummaryEvent.*
import java.math.BigDecimal
import com.spyrosk.expensetracker.utils.TransactionDetailsFragmentTitleProvider.Companion.getTitleForExisting as getTitleForExisting
import com.spyrosk.expensetracker.utils.TransactionDetailsFragmentTitleProvider.Companion.getTitleForNew as getTitleForNew

@AndroidEntryPoint
class SummaryFragment : Fragment(R.layout.fragment_summary) {
	
	private val viewModel: SummaryViewModel by viewModels()
	private lateinit var binding: FragmentSummaryBinding
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		
		binding = FragmentSummaryBinding.bind(view)
		
		viewModel.totalExpenses.observe(viewLifecycleOwner) { bd ->
			updateTextView(binding.totals.tvTotalExpenses, bd)
		}
		viewModel.totalIncomes.observe(viewLifecycleOwner) { bd ->
			updateTextView(binding.totals.tvTotalIncome, bd)
		}
		
		val transactionsAdapter = TransactionsAdapter(compact = true) { t -> onTransactionClicked(t) }
		binding.summaryList.rcvResentTransactions.adapter = transactionsAdapter
		binding.summaryList.rcvResentTransactions.layoutManager = LinearLayoutManager(requireContext())
		viewModel.recentTransactionsWithCategories.observe(viewLifecycleOwner) { list ->
			onTransactionsChange(transactionsAdapter, list)
		}
		
		binding.fabAddTransaction.inflate(R.menu.menu_add_transaction)
		binding.fabAddTransaction.setOnActionSelectedListener(this::onFabActionSelected)
		
		binding.summaryList.imgShowAll.setOnClickListener { viewModel.onShowAllClicked() }
		
		listenToViewModelEvents()
	}
	
	private fun updateTextView(tv: TextView, amount: BigDecimal?) {
		tv.text = CurrencyFormatter.format(amount ?: BigDecimal.ZERO, isNegative = false, usePlusSign = false)
	}
	
	private fun onTransactionsChange(transactionsAdapter: TransactionsAdapter, list: List<TransactionWithCategory>?, ) {
		transactionsAdapter.submitList(list)
		if(!list.isNullOrEmpty())
			viewModel.transactionsFetched()
		else
			viewModel.transactionsMissing()
	}
	
	private fun listenToViewModelEvents() {
		viewLifecycleOwner.lifecycleScope.launchWhenStarted {
			viewModel.summaryEventFlow.collect { event -> handleEvent(event) }
		}
	}
	
	private fun handleEvent(event: SummaryViewModel.SummaryEvent) {
		when(event) {
			is NavigateToAddTransactionScreen -> {
				val action = SummaryFragmentDirections.actionGlobalTransactionDetailsFragment(
						event.transactionType.name,
						titleArg = getTitleForNew(requireContext(), event.transactionType))
				findNavController().navigate(action)
			}
			is NavigateToEditTransactionScreen -> {
				val action = SummaryFragmentDirections.actionGlobalTransactionDetailsFragment(
					transactionTypeArg = event.transaction.transactionType.name,
					transactionArg = event.transaction,
					titleArg = getTitleForExisting(requireContext(), event.transaction))
				findNavController().navigate(action)
			}
			is NavigateToTransactionsScreen -> {
				val action = SummaryFragmentDirections.actionSummaryFragmentToTransactionsFragment()
				findNavController().navigate(action)
			}
			is ChangeNoTransactionsMessageVisibility -> {
				if(event.visible)
					binding.summaryList.tvNoTransactionsMessage.visibility = View.VISIBLE
				else
					binding.summaryList.tvNoTransactionsMessage.visibility = View.GONE
			}
		}
	}
	
	private fun onFabActionSelected(item: SpeedDialActionItem): Boolean {
		when(item.id) {
			R.id.act_add_income -> {
				viewModel.onActionAddIncomeClicked()
				binding.fabAddTransaction.close()
				return true
			}
			R.id.act_add_expense -> {
				viewModel.onActionAddExpenseClicked()
				binding.fabAddTransaction.close()
				return true
			}
			else -> return false
		}
	}
	
	private fun onTransactionClicked(transaction: Transaction) {
		viewModel.transactionClicked(transaction)
	}
	
}
