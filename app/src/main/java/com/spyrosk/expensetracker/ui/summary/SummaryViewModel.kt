package com.spyrosk.expensetracker.ui.summary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.spyrosk.expensetracker.data.Transaction
import com.spyrosk.expensetracker.data.TransactionDao
import com.spyrosk.expensetracker.data.TransactionType
import com.spyrosk.expensetracker.data.TransactionWithCategoryDao
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SummaryViewModel @Inject constructor(
	transactionDao: TransactionDao,
	transactionWithCategoryDao: TransactionWithCategoryDao
) : ViewModel() {
	
	private val summaryEventChannel = Channel<SummaryEvent>()
	val summaryEventFlow = summaryEventChannel.receiveAsFlow()
	
	val recentTransactionsWithCategories = transactionWithCategoryDao.get(limit = 15).asLiveData()
	var totalExpenses = transactionDao.getSum(TransactionType.EXPENSE).asLiveData()
	var totalIncomes = transactionDao.getSum(TransactionType.INCOME).asLiveData()
	
	fun onActionAddIncomeClicked() {
		viewModelScope.launch {
			summaryEventChannel.send(SummaryEvent.NavigateToAddTransactionScreen(TransactionType.INCOME))
		}
	}
	
	fun onActionAddExpenseClicked() {
		viewModelScope.launch {
			summaryEventChannel.send(SummaryEvent.NavigateToAddTransactionScreen(TransactionType.EXPENSE))
		}
	}
	
	fun onShowAllClicked() {
		viewModelScope.launch {
			summaryEventChannel.send(SummaryEvent.NavigateToTransactionsScreen)
		}
	}
	
	fun transactionsFetched() {
		viewModelScope.launch {
			summaryEventChannel.send(SummaryEvent.ChangeNoTransactionsMessageVisibility(visible = false))
		}
	}
	
	fun transactionsMissing() {
		viewModelScope.launch {
			summaryEventChannel.send(SummaryEvent.ChangeNoTransactionsMessageVisibility(visible = true))
		}
	}
	
	fun transactionClicked(transaction: Transaction) {
		viewModelScope.launch {
			summaryEventChannel.send(SummaryEvent.NavigateToEditTransactionScreen(transaction))
		}
	}
	
	// Inner classes
	
	sealed class SummaryEvent {
		data class NavigateToAddTransactionScreen(val transactionType: TransactionType) : SummaryEvent()
		data class NavigateToEditTransactionScreen(val transaction: Transaction) : SummaryEvent()
		object NavigateToTransactionsScreen : SummaryEvent()
		data class ChangeNoTransactionsMessageVisibility(val visible: Boolean) : SummaryEvent()
	}
	
}
