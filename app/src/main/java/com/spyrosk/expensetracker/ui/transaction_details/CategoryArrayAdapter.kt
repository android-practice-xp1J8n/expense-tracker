package com.spyrosk.expensetracker.ui.transaction_details

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.spyrosk.expensetracker.R
import com.spyrosk.expensetracker.data.Category
import com.spyrosk.expensetracker.databinding.ItemCategoryBinding

class CategoryArrayAdapter(
	context: Context,
	items: List<Category>
) : ArrayAdapter<Category>(context, R.layout.item_category, R.id.tv_category_name, items) {
	
	override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
		val view = super.getView(position, convertView, parent)
		view.setPadding(view.paddingLeft / 2, view.paddingTop, view.paddingRight, view.paddingBottom)
		manipulateView(view, position)
		return view
	}
	
	override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
		val view = super.getDropDownView(position, convertView, parent)
		manipulateView(view, position)
		return view
	}
	
	private fun manipulateView(view: View, position: Int) {
		val binding = ItemCategoryBinding.bind(view)
		val category = getItem(position)!!
		binding.imgSwatch.drawable.setTint(category.color)
		binding.tvCategoryName.text = category.name
	}
	
}
