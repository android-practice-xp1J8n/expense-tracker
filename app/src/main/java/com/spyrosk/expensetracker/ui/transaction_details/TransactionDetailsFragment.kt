package com.spyrosk.expensetracker.ui.transaction_details

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.spyrosk.expensetracker.R
import com.spyrosk.expensetracker.data.Category
import com.spyrosk.expensetracker.databinding.FragmentTransactionDetailsBinding
import com.spyrosk.expensetracker.utils.CurrencyInputFilter
import com.spyrosk.expensetracker.utils.dateToMillis
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.math.BigDecimal
import com.spyrosk.expensetracker.utils.CurrencyFormatter.Companion.formatPlain as formatCurrencyPlain

@AndroidEntryPoint
class TransactionDetailsFragment : Fragment(R.layout.fragment_transaction_details) {
	
	private val viewModel: TransactionDetailsViewModel by viewModels()
	private lateinit var binding: FragmentTransactionDetailsBinding
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		
		binding = FragmentTransactionDetailsBinding.bind(view)
		
		viewModel.transactionTitle?.let { binding.etTransactionTitle.setText(it) }
		viewModel.transactionAmount?.let { binding.etTransactionAmount.setText(formatCurrencyPlain(it)) }
		binding.calTransactionDate.date = viewModel.transactionDate		// date will at worst be today's date
		
		binding.fabSave.setOnClickListener { viewModel.onSaveClicked() }
		
		binding.etTransactionTitle.addTextChangedListener { t -> viewModel.transactionTitle = t.toString() }
		binding.etTransactionAmount.addTextChangedListener { t ->
			viewModel.transactionAmount = if(t.toString().isNotBlank()) BigDecimal(t.toString()) else null
		}
		binding.etTransactionAmount.filters = arrayOf(CurrencyInputFilter())
		
		binding.calTransactionDate.setOnDateChangeListener { _, year, month, day ->
			viewModel.transactionDate = dateToMillis(year, month, day)
		}
		
		populateCategorySpinner()
		binding.spTransactionCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
			override fun onNothingSelected(parent: AdapterView<*>?) {}
			override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
				viewModel.transactionCategoryId =
					(binding.spTransactionCategory.adapter.getItem(position) as Category).id
			}
		}
		
		listenToViewModelEvents()
	}
	
	private fun populateCategorySpinner() {
		val spinnerAdapter = CategoryArrayAdapter(requireContext(), arrayListOf())
		binding.spTransactionCategory.adapter = spinnerAdapter
		
		viewModel.availableCategoriesLiveData.observe(viewLifecycleOwner) { newCategories ->
			spinnerAdapter.clear()
			spinnerAdapter.addAll(newCategories)
			
			if(viewModel.transactionCategoryId != null) {
				val selectedIndex = newCategories.indexOfLast { category -> category.id == viewModel.transactionCategoryId }
				if(selectedIndex != -1) binding.spTransactionCategory.setSelection(selectedIndex)
			}
		}
	}
	
	private fun listenToViewModelEvents() {
		viewLifecycleOwner.lifecycleScope.launchWhenStarted {
			viewModel.transactionDetailsEventFlow.collect { event -> handleEvent(event) }
		}
	}
	
	private fun handleEvent(event: TransactionDetailsViewModel.TransactionDetailsEvent) {
		when(event) {
			is TransactionDetailsViewModel.TransactionDetailsEvent.InvalidInput -> {
				Snackbar.make(requireView(), getString(R.string.transaction_details_invalid_input_msg), Snackbar.LENGTH_LONG).show()
			}
			is TransactionDetailsViewModel.TransactionDetailsEvent.AmountIsZero -> {
				Snackbar.make(requireView(), getString(R.string.transaction_details_amount_zero_msg), Snackbar.LENGTH_LONG).show()
			}
			is TransactionDetailsViewModel.TransactionDetailsEvent.NavigateBack -> {
				findNavController().popBackStack()
			}
		}
	}
	
}
