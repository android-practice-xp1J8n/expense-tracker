package com.spyrosk.expensetracker.ui.transaction_details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.spyrosk.expensetracker.data.CategoryDao
import com.spyrosk.expensetracker.data.Transaction
import com.spyrosk.expensetracker.data.TransactionDao
import com.spyrosk.expensetracker.data.TransactionType
import com.spyrosk.expensetracker.utils.currentDateInMillis
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.math.BigDecimal
import javax.inject.Inject

@HiltViewModel
class TransactionDetailsViewModel @Inject constructor(
	private val transactionDao: TransactionDao,
	categoryDao: CategoryDao,
	private val savedState: SavedStateHandle
) : ViewModel() {
	
	private val transactionTitleKey = "transaction_title_key"
	private val transactionAmountKey = "transaction_amount_key"
	private val transactionDateKey = "transaction_date_key"
	private val transactionCategoryIdKey = "transaction_category_id_key"
	
	private val transactionFromArgs = savedState.get<Transaction>("transaction_arg")
	private val transactionTypeFromArgs = try { TransactionType.valueOf(savedState.get<String>("transaction_type_arg") ?: "") }
										  catch(e: Exception) { null }
	
	private val transactionDetailsEventChannel = Channel<TransactionDetailsEvent>()
	val transactionDetailsEventFlow = transactionDetailsEventChannel.receiveAsFlow()
	
	var transactionTitle = savedState.get<String>(transactionTitleKey) ?: transactionFromArgs?.title
	set(value) {
		field = value
		savedState.set(transactionTitleKey, value)
	}
	
	var transactionAmount = savedState.get<BigDecimal>(transactionAmountKey) ?: transactionFromArgs?.amount
	set(value) {
		field = value
		savedState.set(transactionAmountKey, value)
	}
	
	var transactionDate = savedState.get<Long>(transactionDateKey) ?: transactionFromArgs?.dateTimestamp ?: currentDateInMillis()
	set(value) {
		field = value
		savedState.set(transactionDateKey, value)
	}
	
	var transactionCategoryId = savedState.get<Int>(transactionCategoryIdKey) ?: transactionFromArgs?.category_id
	set(value) {
		field = value
		savedState.set(transactionCategoryIdKey, value)
	}
	
	private val transactionType = transactionTypeFromArgs ?: transactionFromArgs?.transactionType ?: TransactionType.EXPENSE
	
	var availableCategoriesLiveData = categoryDao.getAllForType(transactionType).asLiveData()
	
	
	fun onSaveClicked() {
		if(inputIsInvalid()) {
			viewModelScope.launch {
				transactionDetailsEventChannel.send(TransactionDetailsEvent.InvalidInput)
			}
			return
		}
		
		// TODO check for number range
		if(transactionAmount == BigDecimal.ZERO) {
			viewModelScope.launch {
				transactionDetailsEventChannel.send(TransactionDetailsEvent.AmountIsZero)
			}
			return
		}
		
		viewModelScope.launch {
			if(transactionFromArgs != null) {
				transactionDao.update(transactionFromArgs.copy(
					title = transactionTitle!!,
					transactionType = transactionType,
					amount = transactionAmount!!,
					dateTimestamp = transactionDate,
					category_id = transactionCategoryId!!))
			}
			else {
				transactionDao.insert(Transaction(
					title = transactionTitle!!,
					transactionType = transactionType,
					amount = transactionAmount!!,
					dateTimestamp = transactionDate,
					category_id = transactionCategoryId!!))
			}
			transactionDetailsEventChannel.send(TransactionDetailsEvent.NavigateBack)
		}
		
	}
	
	private fun inputIsInvalid() =
		transactionTitle == null || transactionAmount == null || transactionTitle!!.isBlank() ||
				transactionCategoryId == null	// shouldn't happen unless the corresponding view has not been initialized
	
	// Inner classes
	
	sealed class TransactionDetailsEvent {
		object InvalidInput : TransactionDetailsEvent()
		object NavigateBack : TransactionDetailsEvent()
		object AmountIsZero : TransactionDetailsEvent()
	}
	
}
