package com.spyrosk.expensetracker.ui.transactions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.spyrosk.expensetracker.data.Transaction
import com.spyrosk.expensetracker.data.TransactionWithCategory
import com.spyrosk.expensetracker.databinding.ItemTransactionBinding
import com.spyrosk.expensetracker.databinding.ItemTransactionCompactBinding

class TransactionsAdapter(
	private val compact: Boolean = false,
	private val onClickListener: OnTransactionClickListener
) : ListAdapter<TransactionWithCategory, TransactionsAdapter.TransactionViewHolder>(TransactionDiffCallback()) {
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
		return if(!compact) {
			val binding = ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
			TransactionFullViewHolder(binding)
		}
		else {
			val binding = ItemTransactionCompactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
			TransactionCompactViewHolder(binding)
		}
	}
	
	override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
		holder.bind(getItem(position))
	}
	
	// Inner classes
	
	fun interface OnTransactionClickListener {
		fun onTransactionClicked(transaction: Transaction)
	}
	
	abstract inner class TransactionViewHolder(binding: ViewBinding) : RecyclerView.ViewHolder(binding.root) {
		init {
			binding.root.setOnClickListener {
				if(adapterPosition != RecyclerView.NO_POSITION)
					onClickListener.onTransactionClicked(getItem(adapterPosition).transaction)
			}
		}
		
		abstract fun bind(transactionWithCategory: TransactionWithCategory)
	}
	
	inner class TransactionFullViewHolder(private val binding: ItemTransactionBinding) : TransactionViewHolder(binding) {
		
		override fun bind(transactionWithCategory: TransactionWithCategory) {
			binding.tvTransactionTitle.text = transactionWithCategory.transaction.title
			binding.tvTransactionAmount.text = transactionWithCategory.transaction.amountFormatted
			binding.tvTransactionDate.text = transactionWithCategory.transaction.dateTimestampFormatted
			
			binding.tvTransactionCategory.text = transactionWithCategory.category.name
			binding.imgCategorySwatch.drawable.setTint(transactionWithCategory.category.color)
		}
		
	}
	
	inner class TransactionCompactViewHolder(private val binding: ItemTransactionCompactBinding) : TransactionViewHolder(binding) {
		
		override fun bind(transactionWithCategory: TransactionWithCategory) {
			binding.tvTransactionTitle.text = transactionWithCategory.transaction.title
			binding.tvTransactionAmount.text = transactionWithCategory.transaction.amountFormatted
			
			binding.imgCategorySwatch.drawable.setTint(transactionWithCategory.category.color)
		}
		
	}
	
	class TransactionDiffCallback : DiffUtil.ItemCallback<TransactionWithCategory>() {
		override fun areItemsTheSame(oldItem: TransactionWithCategory, newItem: TransactionWithCategory) =
			(oldItem.transaction.id == newItem.transaction.id)
		
		override fun areContentsTheSame(oldItem: TransactionWithCategory, newItem: TransactionWithCategory) =
			(oldItem == newItem)
	}
	
}
