package com.spyrosk.expensetracker.ui.transactions

import androidx.lifecycle.*
import com.spyrosk.expensetracker.data.*
import com.spyrosk.expensetracker.ui.filters.FiltersParcelable
import com.spyrosk.expensetracker.utils.setSetValueDistinct
import com.spyrosk.expensetracker.utils.setValueDistinct
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.math.BigDecimal
import javax.inject.Inject

@HiltViewModel
class TransactionsViewModel @Inject constructor(
	private val transactionDao: TransactionDao,
	private val transactionWithCategoryDao: TransactionWithCategoryDao
) : ViewModel() {
	
	private val transactionsEventChannel = Channel<TransactionsEvent>()
	val transactionsEventFlow = transactionsEventChannel.receiveAsFlow()
	
	private val categoriesFilterLiveData = MutableLiveData<Set<Int>?>(null)	// A Set is used to help simplify equality checks
	private val amountMinFilterLiveData = MutableLiveData<BigDecimal?>(null)
	private val amountMaxFilterLiveData = MutableLiveData<BigDecimal?>(null)
	private val dateStartFilterLiveData = MutableLiveData<Long?>(null)
	private val dateEndFilterLiveData = MutableLiveData<Long?>(null)
	private val orderDescendingLiveData = MutableLiveData(true)	// This probably should have an initial value
	
	private val transactionsWithCategoriesFlow =
		combine(categoriesFilterLiveData.asFlow(),
			amountMinFilterLiveData.asFlow(), amountMaxFilterLiveData.asFlow(),
			dateStartFilterLiveData.asFlow(), dateEndFilterLiveData.asFlow(), orderDescendingLiveData.asFlow()
		)
		{ categories, amountMin, amountMax, dateStart, dateEnd, orderDescending ->
			TransactionsParamCombination(categories, amountMin, amountMax, dateStart, dateEnd, orderDescending)
		}
		.flatMapLatest { (categories, amountMin, amountMax, dateStart, dateEnd, orderDescending) ->
			transactionWithCategoryDao.get(
				categoryIds = categories,
				amountMin = amountMin,
				amountMax = amountMax,
				dateStart = dateStart,
				dateEnd = dateEnd,
				orderDescending = orderDescending
			)
		}
	
	val transactionsWithCategoriesLiveData = transactionsWithCategoriesFlow.asLiveData()
	
	
	fun onTransactionClicked(transaction: Transaction) {
		viewModelScope.launch {
			transactionsEventChannel.send(TransactionsEvent.NavigateToEditTransactionScreen(transaction))
		}
	}
	
	fun onActionAddIncomeClicked() {
		viewModelScope.launch {
			transactionsEventChannel.send(TransactionsEvent.NavigateToAddTransactionScreen(
				TransactionType.INCOME))
		}
	}
	
	fun onActionAddExpenseClicked() {
		viewModelScope.launch {
			transactionsEventChannel.send(TransactionsEvent.NavigateToAddTransactionScreen(
				TransactionType.EXPENSE))
		}
	}
	
	fun onTransactionSwiped(transaction: Transaction) {
		viewModelScope.launch {
			transactionDao.delete(transaction)
			transactionsEventChannel.send(TransactionsEvent.ShowUndoTransactionDeleteMessage(transaction))
		}
	}
	
	fun onTransactionDeleteUndone(transaction: Transaction) {
		viewModelScope.launch {
			transactionDao.insert(transaction)
		}
	}
	
	fun onActionFilterSelected() {
		viewModelScope.launch {
			transactionsEventChannel.send(TransactionsEvent.NavigateToFilterDialog)
		}
	}
	
	fun transactionsFetched() {
		viewModelScope.launch {
			transactionsEventChannel.send(TransactionsEvent.ChangeNoTransactionsMessageVisibility(visible = false))
		}
	}
	
	fun transactionsMissing() {
		viewModelScope.launch {
			transactionsEventChannel.send(TransactionsEvent.ChangeNoTransactionsMessageVisibility(visible = true))
		}
	}
	
	fun filterResultsReceived(filters: FiltersParcelable) {
		if(filters.filtersCleared)
			clearFilters()
		else {
			categoriesFilterLiveData.setSetValueDistinct(filters.selectedCategoryIDs)
			amountMinFilterLiveData.setValueDistinct(filters.amountMin)
			amountMaxFilterLiveData.setValueDistinct(filters.amountMax)
			dateStartFilterLiveData.setValueDistinct(filters.dateStart)
			dateEndFilterLiveData.setValueDistinct(filters.dateEnd)
		}
	}
	
	fun clearFilters() {
		categoriesFilterLiveData.setValueDistinct(null)
		amountMinFilterLiveData.setValueDistinct(null)
		amountMaxFilterLiveData.setValueDistinct(null)
		dateStartFilterLiveData.setValueDistinct(null)
		dateEndFilterLiveData.setValueDistinct(null)
	}
	
	fun onActionSortDescendingSelected() {
		orderDescendingLiveData.setValueDistinct(true)
	}
	
	fun onActionSortAscendingSelected() {
		orderDescendingLiveData.setValueDistinct(false)
	}
	
	fun createFiltersParcelable() =
		FiltersParcelable(
			categoriesFilterLiveData.value?.toMutableSet(),
			amountMinFilterLiveData.value,
			amountMaxFilterLiveData.value,
			dateStartFilterLiveData.value,
			dateEndFilterLiveData.value,
		)
	
	// Inner classes
	
	sealed class TransactionsEvent {
		data class NavigateToEditTransactionScreen(val transaction: Transaction) : TransactionsEvent()
		data class ShowUndoTransactionDeleteMessage(val transaction: Transaction) : TransactionsEvent()
		data class NavigateToAddTransactionScreen(val transactionType: TransactionType) : TransactionsEvent()
		object NavigateToFilterDialog : TransactionsEvent()
		data class ChangeNoTransactionsMessageVisibility(val visible: Boolean) : TransactionsEvent()
	}
	
}
