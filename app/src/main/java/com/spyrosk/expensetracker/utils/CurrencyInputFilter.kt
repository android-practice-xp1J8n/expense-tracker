package com.spyrosk.expensetracker.utils

import android.text.InputFilter
import android.text.Spanned

class CurrencyInputFilter : InputFilter {
	
	override fun filter(source: CharSequence?, start: Int, end: Int,
						dest: Spanned?, dstart: Int, dend: Int): CharSequence {
		if(source == null)
			return ""
		
		if(source.isEmpty())
			return source
		
		val initialText = dest.toString()
		val dotIndex = initialText.indexOf(".")
		if(dotIndex == -1 || dstart <= dotIndex)
			return source
		
		// At this point it is certain that there IS a dot in the original text ...
		// ... and that the new input is being inserted after that dot
		val sourceString = source.toString().replace(".", "")
		val lengthOfNewInput = sourceString.length
		val existingDecimals = initialText.length - dotIndex - 1
		val maxAllowedDecimals = 2
		val decimalPlacesLeft = maxAllowedDecimals - existingDecimals
		
		if(lengthOfNewInput <= decimalPlacesLeft)
			return sourceString
		else
			return sourceString.substring(0, decimalPlacesLeft)
	}
	
}
