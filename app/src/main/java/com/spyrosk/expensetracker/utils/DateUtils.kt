package com.spyrosk.expensetracker.utils

import java.text.DateFormat
import java.util.*

fun dateToMillis(year: Int, month: Int, day: Int): Long {
	val c: Calendar = Calendar.getInstance()
	c.set(year, month, day, 0, 0, 0)
	// The milliseconds are set to 1 just to ensure the date is in fact "in" the desired day
	c.set(Calendar.MILLISECOND, 1)
	return c.timeInMillis
}

fun currentDateInMillis(): Long {
	val c: Calendar = Calendar.getInstance()	// this calls System.currentTimeMillis()
	return dateToMillis(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH))
}

fun formatMedium(dateLong: Long): String {
	return DateFormat.getDateInstance(DateFormat.MEDIUM).format(dateLong)
}

fun formatLong(dateLong: Long): String {
	return DateFormat.getDateInstance(DateFormat.LONG).format(dateLong)
}
