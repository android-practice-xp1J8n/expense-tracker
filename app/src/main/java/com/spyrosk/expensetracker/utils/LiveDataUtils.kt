package com.spyrosk.expensetracker.utils

import androidx.lifecycle.MutableLiveData

fun MutableLiveData<Set<Int>?>.setSetValueDistinct(newSet: Set<Int>?) {
	val oldSet = this.value
	
	if(oldSet == newSet)
		return
	
	// The case in which both are null is covered above
	if(oldSet == null || newSet == null) {
		this.value = newSet
		return
	}
	
	if(oldSet.size != newSet.size) {
		this.value = newSet
		return
	}
	
	// At this point it is certain that both sets i) are not null and and ii) have the same size
	var sameContents = true
	for(newSetItem in newSet) {
		if(!oldSet.contains(newSetItem)) {
			sameContents = false
			break
		}
	}
	
	if(!sameContents)
		this.value = newSet
}

fun <T> MutableLiveData<T>.setValueDistinct(newValue: T) {
	if(this.value != newValue)
		this.value = newValue
}
