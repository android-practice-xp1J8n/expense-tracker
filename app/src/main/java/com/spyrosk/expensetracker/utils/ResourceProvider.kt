package com.spyrosk.expensetracker.utils

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class ResourceProvider @Inject constructor(@ApplicationContext private val context: Context) {
	
	fun getColor(colorId: Int): Int = getColorOpt(context, colorId)
	
	fun getString(stringId: Int): String = context.getString(stringId)
	
}
