![Project Logo](assets/logo.png)

<a href="https://play.google.com/store/apps/details?id=com.spyrosk.expensetracker"><img src="assets/get_it_on_google_play.png" width="150"></a>

## Description
Write down the details of your everyday incomes and expenses to easily access them in the future.

## Screenshots
[<img src="assets/scr1.jpg" width=160>](assets/scr1.jpg)
[<img src="assets/scr2.jpg" width=160>](assets/scr2.jpg)
[<img src="assets/scr3.jpg" width=160>](assets/scr3.jpg)
[<img src="assets/scr5.jpg" width=160>](assets/scr5.jpg)
[<img src="assets/scr6.jpg" width=160>](assets/scr6.jpg)
[<img src="assets/scr7.jpg" width=160>](assets/scr7.jpg)
[<img src="assets/scr8.jpg" width=160>](assets/scr8.jpg)
